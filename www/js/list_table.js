/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
        // document.getElementById("cameraTakePicture").addEventListener("click", cameraTakePicture);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        //디바이스가 준비 되면 버튼 클릭시 카메라 어플 사용
        //document.getElementsById('camera').addEventListener('click', this.cameraTakePicture, false)
    },
    //---------------------------------------
    //로그아웃시 자동 로그인 해제
    onDeleteAutoLogin:function(email){
        var db = window.sqlitePlugin.openDatabase({name: 'my.db', location: 'default'});
        db.transaction(function (tx) {

            var query = "DELETE FROM auto_login WHERE email = ?";

            tx.executeSql(query, [email], function (tx, res) {
                    console.log("removeId: " + res.insertId);
                    console.log("rowsAffected: " + res.rowsAffected);
                    location.href="index.html"
                },
                function (tx, error) {
                    console.log('DELETE error: ' + error.message);
                    location.href="index.html"
                });
        }, function (error) {
            console.log('transaction error: ' + error.message);
        }, function () {
            console.log('transaction ok');
        });
    },
    //---------------------------------------

    //----------------------------------------
    //카메라 관련 이벤트
    cameraTakePicture:function(){
        navigator.camera.getPicture(this.onSuccess, this.onFail, {
            correctOrientation: true,
            saveToPhotoAlbum: true,
        });
    },
    AlbumTakePicture:function(){
        navigator.camera.getPicture(this.onSuccess, this.onFail, {
            correctOrientation: true,
            saveToPhotoAlbum: true,
            sourceType: navigator.camera.PictureSourceType.SAVEDPHOTOALBUM
        });
    },
    onSuccess: function(imageData) {
        var image = document.getElementById('modal_blah');
        image.src = /*"data:image/jpeg;base64," + */imageData;
        //$('#imgInp').src = imageData;
        var fileURL = imageData;
        $('#addModal').modal('show');
        $('#register').click(function (event) {
            $('#register').attr("disabled", true)
            //---------------------------------------------------
            //file API
            $('#uploadModal').modal('show');
            function win(r) {
                console.log("Code = " + r.responseCode);
                console.log("Sent = " + r.bytesSent);
                if (r.response < 0) {
                    if (r.response == -1) {
                        alert("Server Error!!!")
                    } else {
                        alert("Client Error!!! Try Again!")
                    }
                }else {
                    $('#login').attr("disabled", false)
                    $('#uploadModal').modal('hide')
                    reload()
                }
            }

            function fail(error) {
                alert("An error has occurred: Code = " + error.code);
                $('#login').attr("disabled", false)
                $('#uploadModal').modal('hide')
                console.log("upload error source " + error.source);
                console.log("upload error target " + error.target);
            }

            var options = new FileUploadOptions();
            var params = {};
            params.category = $('#modal_category option:selected').val()
            params.name = $('#modal_name').val();

            options.params = params;

            var ft = new FileTransfer();

            ft.upload(fileURL, encodeURI(serv_path + "image_upload"), win, fail, options);
        })
    },

    onFail: function(message) {
        console.log('Failed because: ' + message);
    }
    //----------------------------------------
};

app.initialize();


/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        //this.receivedEvent('deviceready');
        //db.executeSql('DROP TABLE IF EXISTS test_table');

        console.log("Device ready!!")
        document.addEventListener("backbutton", this.onBackKeyDown, true);
        onload();
    },
    onSelectAutoLogin:function () {
        var db = window.sqlitePlugin.openDatabase({name: 'my.db', location: 'default'});
        db.transaction(function (tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS auto_login (email text primary key, passwd text)');
            tx.executeSql("SELECT * FROM auto_login", [], function(tx, res){
                console.log(res.rows.item(0).email)
                $('.login-box-body').hide(10, function () {
                    $('#loginModal').modal('show', {backdrop: 'static', keyboard: false});
                });
                autologin(res.rows.item(0).email, res.rows.item(0).passwd)
            }, function (e) {
                console.log("ERROR: " + e.message);
            });
        },function (error) {
            console.log('transaction error: ' + error.message);
        }, function () {
            console.log('transaction ok');
        });
    },
    onInsertAutoLogin:function(email, passwd){
        var db = window.sqlitePlugin.openDatabase({name: 'my.db', location: 'default'});

        db.transaction(function(tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS auto_login (email text primary key, passwd text)');
            tx.executeSql("INSERT INTO auto_login (email, passwd) VALUES (?,?)", [email, passwd], function (tx, res) {
                console.log("insertId: " + res.insertId + " -- probably 1");
                console.log("rowsAffected: " + res.rowsAffected + " -- should be 1");
                location.href="list_table.html?q=1";
            }, function (e) {
                console.log("ERROR: " + e.message);
                location.href="list_table.html?q=1";
            })
        },function (error) {
            console.log('transaction error: ' + error.message);
        }, function () {
            console.log('transaction ok');
        });
    },
    onDeleteAutoLogin:function(email){
        var db = window.sqlitePlugin.openDatabase({name: 'my.db', location: 'default'});
        db.transaction(function (tx) {

            var query = "DELETE FROM auto_login WHERE email = ?";

            tx.executeSql(query, [email], function (tx, res) {
                    console.log("removeId: " + res.insertId);
                    console.log("rowsAffected: " + res.rowsAffected);
                },
                function (tx, error) {
                    console.log('DELETE error: ' + error.message);
                });
        }, function (error) {
            console.log('transaction error: ' + error.message);
        }, function () {
            console.log('transaction ok');
        });
    },
    //---------------------------------------
    //뒤로가기 버튼 이벤트
    onBackKeyDown: function() {
        navigator.app.exitApp();
    }
    //---------------------------------------
};

app.initialize();

